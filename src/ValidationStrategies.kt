package com.easy

import arrow.core.Either
import arrow.core.Nel
import arrow.core.Validated
import arrow.core.extensions.either.applicativeError.applicativeError
import arrow.core.extensions.nonemptylist.semigroup.semigroup
import arrow.core.extensions.validated.applicativeError.applicativeError

fun <E> failFast() = Either.applicativeError<Nel<E>>()
fun <E> accumulateErrors() = Validated.applicativeError<Nel<E>>(Nel.semigroup())