package com.easy

import arrow.Kind
import arrow.core.extensions.list.foldable.foldLeft
import arrow.typeclasses.MonadError
import arrow.typeclasses.Show
import java.math.BigDecimal

enum class LogicError {
    UnknownCourseConversion,
    SimpleTaxRateNotFound,
    ProgressiveTaxRateNotFound,
    NoTaxInformation
}

object ShowLogicError : Show<LogicError> {
    override fun LogicError.show(): String =
        when (this) {
            LogicError.UnknownCourseConversion -> "Unknown course conversion!"
            LogicError.SimpleTaxRateNotFound -> "Simple tax rate not found!"
            LogicError.ProgressiveTaxRateNotFound -> "ProgressiveTaxRateNotFound!"
            LogicError.NoTaxInformation -> "No tax information found for mode ToNet or ToGross!"
        }
}

suspend fun <F> MonadError<F, LogicError>.calculate(request: SalaryRequest) = this.fx.monad {
    val course = !getCourse(request.originalCurrency, request.targetCurrency)
    val taxFactor = !calculateTaxFactor(request, course)
    val adjustedByTaxAmount = when (request.calculationMode) {
        CalculationMode.ToNet -> request.annualAmount * taxFactor
        CalculationMode.ToGross -> request.annualAmount / taxFactor
        CalculationMode.AsIs -> request.annualAmount
    }
    val (inOriginalCurrencyGross, inOriginalCurrencyNet) =
        (if (request.calculationMode == CalculationMode.ToGross)
            adjustedByTaxAmount to request.annualAmount
        else
            request.annualAmount to adjustedByTaxAmount).fromAnnual()
    SalaryResponse(
        inOriginalCurrencyGross = inOriginalCurrencyGross,
        inOriginalCurrencyNet = inOriginalCurrencyNet,
        inTargetCurrency = (course * adjustedByTaxAmount).fromAnnual()
    )
}

fun <F> MonadError<F, LogicError>.calculateTaxFactor(request: SalaryRequest, course: BigDecimal): Kind<F, BigDecimal> {
    return if (request.calculationMode == CalculationMode.AsIs) BigDecimal.ONE.just()
    else when (val taxInformation = request.taxInformation) {
        is TaxInformation.Country -> taxAdjustmentFractionByCountry(
            country = taxInformation.value,
            annualAmount = request.annualAmount
                .adjustWithCourseAndMode(course, request.calculationMode)
        )
        is TaxInformation.CustomTaxRate -> just(BigDecimal.ONE - taxInformation.value * 0.01f.toBigDecimal())
        null -> raiseError(LogicError.NoTaxInformation)
    }
}

fun BigDecimal.adjustWithCourseAndMode(course: BigDecimal, mode: CalculationMode): BigDecimal =
    when (mode) {
        CalculationMode.ToGross -> this * course
        else -> this
    }

fun <F> MonadError<F, LogicError>.taxAdjustmentFractionByCountry(country: Country, annualAmount: BigDecimal): Kind<F, BigDecimal> =
    when (country.taxationType) {
        TaxationType.Simple ->
            simpleTaxationTypes[country]
                ?.let { simpleTaxRate -> BigDecimal.ONE - simpleTaxRate }
                ?.just() ?: raiseError(LogicError.SimpleTaxRateNotFound)
        TaxationType.Progressive ->
            defineProgressiveTaxScale(country)
                ?.let { BigDecimal.ONE - calculateProgressiveTaxRate(it, annualAmount) }
                ?.just() ?: raiseError(LogicError.ProgressiveTaxRateNotFound)
    }

fun defineProgressiveTaxScale(country: Country): List<ProgressiveScaleSlice>? =
    progressiveTaxScales[country]

fun calculateProgressiveTaxRate(scale: List<ProgressiveScaleSlice>, annualAmount: BigDecimal): BigDecimal =
    scale.foldLeft(annualAmount to BigDecimal.ZERO) { (leftAmount, totalTax), (limit, taxRate) ->
        if (leftAmount <= BigDecimal.ZERO) leftAmount to totalTax
        else {
            val currentSliceTax = taxRate * leftAmount.min(limit)
            (leftAmount - limit) to (totalTax + currentSliceTax)
        }
    }.second.let { it / annualAmount }

fun BigDecimal.toAnnual(from: Period): BigDecimal =
    when (from) {
        Period.Year -> this
        Period.Month -> this * MONTHS
        Period.Day -> this * WORKING_DAYS * MONTHS
        Period.Hour -> this * WORKING_HOURS * WORKING_DAYS * MONTHS
    }

fun BigDecimal.fromAnnual(): SalaryInAllPeriods {
    val annual = this
    val monthly = annual / MONTHS
    val daily = monthly / WORKING_DAYS
    val hourly = daily / WORKING_HOURS
    return SalaryInAllPeriods(annual, monthly, daily, hourly)
}

fun Pair<BigDecimal, BigDecimal>.fromAnnual(): Pair<SalaryInAllPeriods, SalaryInAllPeriods> =
    first.fromAnnual() to second.fromAnnual()

val MONTHS = 12f.toBigDecimal()
val WORKING_DAYS = 21f.toBigDecimal()
val WORKING_HOURS = 8f.toBigDecimal()

fun <F> MonadError<F, LogicError>.getCourse(originalCurrency: Currency, targetCurrency: Currency): Kind<F, BigDecimal> =
    (courseRelations.find { originalCurrency == it.first }?.third?.just()
        ?: courseRelations.find { targetCurrency == it.first }?.third?.let { 1f / it }?.just()
        ?: LogicError.UnknownCourseConversion.raiseError())
        .map { it.toBigDecimal() }