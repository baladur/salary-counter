package com.easy

fun SalaryResponse.toDTO() =
    SalaryResponseDTO(
        inOriginalCurrencyGross.toDTO(),
        inOriginalCurrencyNet.toDTO(),
        inTargetCurrency.toDTO()
    )

fun SalaryInAllPeriods.toDTO() = SalaryInAllPeriodsDTO(annualAmount, monthlyAmount, dailyAmount, hourlyAmount)