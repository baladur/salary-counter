package com.easy

import java.math.BigDecimal

enum class Currency {
    RUR, EUR, USD, GBP, SEK, PLN
}

enum class Period {
    Year, Month, Day, Hour
}

enum class Country(val taxationType: TaxationType) {
    UK(TaxationType.Progressive),
    Poland(TaxationType.Progressive),
    Netherlands(TaxationType.Progressive),
    France(TaxationType.Progressive),
    US(TaxationType.Progressive),
    Russia(TaxationType.Simple)
}

enum class TaxationType {
    Simple, Progressive
}

enum class CalculationMode {
    ToNet, ToGross, AsIs
}

enum class RURCourse(val course: Float) {
    EUR(92.19f),
    USD(75.96f),
    GBP(104.15f),
    SEK(9.09f),
    PLN(20.36f)
}

val ukTaxScale = listOf(
    ProgressiveScaleSlice(10_600f.toBigDecimal(), BigDecimal.ZERO),
    ProgressiveScaleSlice(31_785f.toBigDecimal(), 0.2f.toBigDecimal()),
    ProgressiveScaleSlice(150_000f.toBigDecimal(), 0.4f.toBigDecimal()),
    ProgressiveScaleSlice(Float.MAX_VALUE.toBigDecimal(), 0.45f.toBigDecimal())
)

val polandTaxScale = listOf(
    ProgressiveScaleSlice(85_528f.toBigDecimal(), 0.18f.toBigDecimal()),
    ProgressiveScaleSlice(Float.MAX_VALUE.toBigDecimal(), 0.32f.toBigDecimal())
)

val netherlandsTaxScale = listOf(
    ProgressiveScaleSlice(20_384f.toBigDecimal(), 0.3665f.toBigDecimal()),
    ProgressiveScaleSlice(68_507f.toBigDecimal(), 0.3810f.toBigDecimal()),
    ProgressiveScaleSlice(Float.MAX_VALUE.toBigDecimal(), 0.5175f.toBigDecimal())
)

val franceTaxScale = listOf(
    ProgressiveScaleSlice(27_519f.toBigDecimal(), 0.2f.toBigDecimal()),
    ProgressiveScaleSlice(Float.MAX_VALUE.toBigDecimal(), 0.3f.toBigDecimal())
)

val usTaxScale = listOf(
    ProgressiveScaleSlice(9_875f.toBigDecimal(), 0.1f.toBigDecimal()),
    ProgressiveScaleSlice(40_125f.toBigDecimal(), 0.12f.toBigDecimal()),
    ProgressiveScaleSlice(85_525f.toBigDecimal(), 0.22f.toBigDecimal()),
    ProgressiveScaleSlice(163_300f.toBigDecimal(), 0.24f.toBigDecimal()),
    ProgressiveScaleSlice(Float.MAX_VALUE.toBigDecimal(), 0.32f.toBigDecimal())
)

val simpleTaxationTypes = mapOf(
    Country.Russia to 0.13f.toBigDecimal()
)

val progressiveTaxScales = mapOf(
    Country.UK to ukTaxScale,
    Country.Netherlands to netherlandsTaxScale,
    Country.Poland to polandTaxScale,
    Country.France to franceTaxScale,
    Country.US to usTaxScale
)

val courseRelations = listOf(
    Triple(Currency.EUR, Currency.RUR, RURCourse.EUR.course),
    Triple(Currency.USD, Currency.RUR, RURCourse.USD.course),
    Triple(Currency.GBP, Currency.RUR, RURCourse.GBP.course),
    Triple(Currency.SEK, Currency.RUR, RURCourse.SEK.course),
    Triple(Currency.PLN, Currency.RUR, RURCourse.PLN.course)
)

data class ProgressiveScaleSlice(
    val limit: BigDecimal,
    val taxRate: BigDecimal
)

data class SalaryRequest(
    val annualAmount: BigDecimal,
    val originalCurrency: Currency,
    val targetCurrency: Currency,
    val calculationMode: CalculationMode = CalculationMode.AsIs,
    val taxInformation: TaxInformation?
)

sealed class TaxInformation {
    data class Country(val value: com.easy.Country) : TaxInformation()
    data class CustomTaxRate(val value: BigDecimal) : TaxInformation()
}

data class SalaryInAllPeriods(
    val annualAmount: BigDecimal,
    val monthlyAmount: BigDecimal,
    val dailyAmount: BigDecimal,
    val hourlyAmount: BigDecimal
)

data class SalaryResponse(
    val inOriginalCurrencyGross: SalaryInAllPeriods,
    val inOriginalCurrencyNet: SalaryInAllPeriods,
    val inTargetCurrency: SalaryInAllPeriods
)