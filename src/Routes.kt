package com.easy

import arrow.core.Either
import arrow.core.Nel
import arrow.core.extensions.either.monadError.monadError
import arrow.core.fix
import arrow.core.nel
import arrow.typeclasses.Show
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Routing.calculateSalaryInfo() =
    post("/salary-info") {
        val requestDTO = call.receive<SalaryRequestDTO>()
        accumulateErrors<ValidationError>()
            .validateRequest(requestDTO).fix()
            .fold(
                { ShowValidationError.respondErrors(call, it, ErrorType.Simple) },
                { domainValidationResult ->
                    domainValidationResult.fix().fold(
                        { ShowValidationError.respondErrors(call, it, ErrorType.Domain) },
                        { validRequest ->
                            Either.monadError<LogicError>().calculate(validRequest).fix()
                                .fold(
                                    { ShowLogicError.respondErrors(call, it.nel(), ErrorType.Logic) },
                                    { call.respond(it.toDTO()) }
                                )
                        }
                    )
                }
            )
    }

suspend fun <E> Show<E>.respondErrors(call: ApplicationCall, errors: Nel<E>, errorType: ErrorType) =
    call.respond(
        HttpStatusCode.BadRequest,
        mapOf(
            "errors" to errors.map { it.show() },
            "errorType" to errorType
        )
    )

enum class ErrorType {
    Simple, Domain, Logic
}