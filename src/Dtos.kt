package com.easy

import java.math.BigDecimal

data class SalaryRequestDTO(
    val amount: BigDecimal? = null,
    val originalCurrency: String? = null,
    val targetCurrency: String? = null,
    val period: String? = null,
    val country: String? = null,
    val calculationMode: String? = null,
    val customTaxPercent: Float? = null
)

data class SalaryResponseDTO(
    val inOriginalCurrencyGross: SalaryInAllPeriodsDTO,
    val inOriginalCurrencyNet: SalaryInAllPeriodsDTO,
    val inTargetCurrency: SalaryInAllPeriodsDTO
)

data class SalaryInAllPeriodsDTO(
    val annualAmount: BigDecimal,
    val monthlyAmount: BigDecimal,
    val dailyAmount: BigDecimal,
    val hourlyAmount: BigDecimal
)