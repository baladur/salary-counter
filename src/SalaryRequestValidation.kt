package com.easy

import arrow.Kind
import arrow.core.Nel
import arrow.core.nel
import arrow.fx.extensions.io.applicative.just
import arrow.fx.extensions.io.applicativeError.raiseError
import arrow.typeclasses.ApplicativeError
import arrow.typeclasses.Show
import java.math.BigDecimal

typealias SalaryRequestErrorHandling<F> = ApplicativeError<F, Nel<ValidationError>>

sealed class ValidationError
data class NoField(val name: RequiredFieldName) : ValidationError()
object NegativeAmount : ValidationError()
object UnknownCurrency : ValidationError()
object UnknownPeriod : ValidationError()
object UnknownCountry : ValidationError()
object UnknownMode : ValidationError()
object CurrenciesMatch : ValidationError()
object NoTaxProvidedForGivenMode : ValidationError()
object BothCountryAndCustomTaxPercentProvided : ValidationError()
object UnexpectedTaxInformationError : ValidationError()
object ToGrossModeIsNotSupportedForProgressiveTaxation : ValidationError()

object ShowValidationError : Show<ValidationError> {
    override fun ValidationError.show(): String =
        when (val a = this) {
            is NoField -> "No field [${a.name}] found"
            NegativeAmount -> "Amount can't be negative!"
            UnknownCurrency -> "Unknown currency!"
            UnknownPeriod -> "Unknown period!"
            UnknownCountry -> "Unknown country!"
            UnknownMode -> "Unknown mode!"
            CurrenciesMatch -> "Original and target currencies shouldn't match!"
            NoTaxProvidedForGivenMode -> "No tax provided for given mode!"
            BothCountryAndCustomTaxPercentProvided -> "Country and customTaxPercent are mutually-exclusive!"
            ToGrossModeIsNotSupportedForProgressiveTaxation -> "ToGross mode is not supported for progressive taxation!"
            UnexpectedTaxInformationError -> "Unexpected error during tax information validation!"
        }
}

enum class RequiredFieldName {
    Amount, OriginalCurrency, TargetCurrency, Period
}

fun <F> SalaryRequestErrorHandling<F>.validateRequest(
    requestDTO: SalaryRequestDTO
): Kind<F, Kind<F, SalaryRequest>> =
    mapN(
        validateAmount(requestDTO.amount),
        validateCurrency(requestDTO.originalCurrency, RequiredFieldName.OriginalCurrency),
        validateCurrency(requestDTO.targetCurrency, RequiredFieldName.TargetCurrency),
        validatePeriod(requestDTO.period),
        validateCountry(requestDTO.country),
        validateCalculationMode(requestDTO.calculationMode),
        validateCustomTaxPercent(requestDTO.customTaxPercent)
    ) { (amount, originalCurrency, targetCurrency, period, country, mode, customTaxPercent) ->
        mapN(
            validateTaxInformation(country, mode, customTaxPercent),
            validateCurrenciesDoNotMatch(originalCurrency, targetCurrency),
            validateModeIsSupported(mode, country)
        ) { (taxInfo) ->
            SalaryRequest(amount.toAnnual(period), originalCurrency, targetCurrency, mode, taxInfo)
        }
    }

// Common
fun <F, A, B> SalaryRequestErrorHandling<F>.validateFieldExists(
    field: A?,
    name: RequiredFieldName,
    ifExists: (A) -> Kind<F, B>
): Kind<F, B> =
    field?.let(ifExists) ?: raiseError(NoField(name).nel())

fun <F> SalaryRequestErrorHandling<F>.validatePositive(amount: BigDecimal): Kind<F, BigDecimal> =
    if (amount < BigDecimal.ZERO) raiseError(NegativeAmount.nel())
    else amount.just()

// Field specific
fun <F> SalaryRequestErrorHandling<F>.validateAmount(rawValue: BigDecimal?): Kind<F, BigDecimal> =
    validateFieldExists(rawValue, RequiredFieldName.Amount, ::validatePositive)

fun <F> SalaryRequestErrorHandling<F>.validateCurrency(
    rawValue: String?,
    fieldName: RequiredFieldName
): Kind<F, Currency> =
    validateFieldExists(rawValue, fieldName) { currency ->
        runCatching { Currency.valueOf(currency) }
            .toApplicativeError(this) { UnknownCurrency.nel() }
    }

fun <F> SalaryRequestErrorHandling<F>.validatePeriod(rawValue: String?): Kind<F, Period> =
    validateFieldExists(rawValue, RequiredFieldName.Period) { period ->
        runCatching { Period.valueOf(period) }
            .toApplicativeError(this) { UnknownPeriod.nel() }
    }

fun <F> SalaryRequestErrorHandling<F>.validateCountry(rawValue: String?): Kind<F, Country?> =
    rawValue?.let { country ->
        runCatching { Country.valueOf(country) }
            .toApplicativeError(this) { UnknownCountry.nel() }
    } ?: just(null)

fun <F> SalaryRequestErrorHandling<F>.validateCalculationMode(rawValue: String?): Kind<F, CalculationMode> =
    rawValue?.let { calculationMode ->
        runCatching { CalculationMode.valueOf(calculationMode) }
            .toApplicativeError(this) { UnknownMode.nel() }
    } ?: CalculationMode.AsIs.just()

fun <F> SalaryRequestErrorHandling<F>.validateCustomTaxPercent(rawValue: Float?): Kind<F, BigDecimal?> =
    rawValue
        ?.toBigDecimal()
        ?.let(::validatePositive)
        ?: just(null)

fun <F> SalaryRequestErrorHandling<F>.validateCurrenciesDoNotMatch(a: Currency, b: Currency): Kind<F, Unit> =
    if (a == b) raiseError(CurrenciesMatch.nel()) else Unit.just()

fun <F> SalaryRequestErrorHandling<F>.validateTaxInformation(
    country: Country?,
    calculationMode: CalculationMode,
    customTaxPercent: BigDecimal?
): Kind<F, TaxInformation?> =
    if (calculationMode != CalculationMode.AsIs) {
        when {
            country == null && customTaxPercent == null -> raiseError(NoTaxProvidedForGivenMode.nel())
            country != null && customTaxPercent != null -> raiseError(BothCountryAndCustomTaxPercentProvided.nel())
            country != null -> TaxInformation.Country(country).just()
            customTaxPercent != null -> TaxInformation.CustomTaxRate(customTaxPercent).just()
            else -> raiseError(UnexpectedTaxInformationError.nel())
        }
    } else just(null)

fun <F> SalaryRequestErrorHandling<F>.validateModeIsSupported(mode: CalculationMode, country: Country?): Kind<F, Unit> =
    if (mode == CalculationMode.ToGross && country?.taxationType == TaxationType.Progressive)
        raiseError(ToGrossModeIsNotSupportedForProgressiveTaxation.nel())
    else
        Unit.just()

fun <F, A, E> Result<A>.toApplicativeError(AE: ApplicativeError<F, E>, e: () -> E): Kind<F, A> =
    this.let { result ->
        with(AE) {
            result.fold(
                { it.just() },
                { e().raiseError() }
            )
        }
    }